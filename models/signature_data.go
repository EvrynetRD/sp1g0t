package models

type SignatureData struct {
	Signature string `json:"signature"`
}
