/*
 * floodgate API
 *
 * Floodgate DEX API
 *
 * API version: 1.0.0
 * Contact: fg-support@evrynet.io
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package models

type ModelError struct {

	// error code
	Code int32 `json:"code"`

	// error message
	Message string `json:"message"`
}
