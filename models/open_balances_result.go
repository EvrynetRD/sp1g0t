/*
 * floodgate API
 *
 * Floodgate DEX API
 *
 * API version: 1.0.0
 * Contact: fg-support@evrynet.io
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package models

type OpenBalancesResult struct {

	// true if the open (orders account) balances were obtained successfully and false otherwise
	Success bool `json:"success"`

	Base OpenBalanceData `json:"base"`

	Quote OpenBalanceData `json:"quote"`
}
