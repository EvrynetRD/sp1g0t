package models

type MarketPairs struct {
	Success bool     `json:"success"`
	Pairs   []string `json:"pairs"`
}
