package models

// //responses for different types of /tx/{tx_id}/sign endpoint

type SignedOrders struct {
	Result
	Orders []OrderData `json:"orders,omitempty"`
}
