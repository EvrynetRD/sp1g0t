/*
 * floodgate API
 *
 * Floodgate DEX API
 *
 * API version: 1.0.0
 * Contact: fg-support@evrynet.io
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package models

type OrderReq struct {

	// identifier supplied by the client
	ClientId string `json:"client_id"`

	// asset pair specifying the base/quote asset the client wishes to trade
	Market string `json:"market"`

	// market address
	MarketAddress string `json:"market_address"`

	// maximum price the client is willing to pay, will be ignored for market orders
	Price string `json:"price"`

	// trading side, may be one of: buy or sell
	Side string `json:"side"`

	// how much shall be bought or sold
	Size string `json:"size"`

	// order type, may be one of: limit, ioc or post_only
	OrderType string `json:"order_type"`
}
