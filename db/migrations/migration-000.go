/*
 * Copyright. PwC Advisory LLC, 2020
 */

package migrations

func init() {
	up := []string{
		`CREATE TABLE markets (
			program_id TEXT PRIMARY KEY,
			market_address TEXT,
			name TEXT,
			base TEXT,
			quote TEXT,
			base_mint_addr TEXT,
			quote_mint_addr TEXT,
			min_order_size TEXT,
			tick_size TEXT
		);`,

		`CREATE TABLE orders (
			id uuid PRIMARY KEY,
			client_id TEXT NOT NULL,
			market TEXT,
			market_address TEXT NOT NULL,
			price TEXT NOT NULL,
			filled_size TEXT,
			size TEXT NOT NULL,
			side TEXT NOT NULL,
			order_type TEXT DEFAULT 'limit',
			tx_type TEXT NOT NULL,
			created_at timestamp without time zone default CURRENT_TIMESTAMP,
			modified_at timestamp without time zone default CURRENT_TIMESTAMP,
			status TEXT NOT NULL DEFAULT 'PENDING',
			tx_id TEXT
		);`,

		`CREATE TABLE settles (
			id uuid PRIMARY KEY,
			client_id TEXT NOT NULL,
			market TEXT,
			market_address TEXT NOT NULL,
			tx_type TEXT NOT NULL,
			created_at timestamp without time zone default (now() at time zone 'utc'),
			modified_at timestamp without time zone default (now() at time zone 'utc'),
			tx_id TEXT
		);`,

		`CREATE TABLE prices (
			id uuid PRIMARY KEY,
			client_id TEXT NOT NULL,
			asset TEXT,
			price TEXT,
			created_at  timestamp without time zone default (now() at time zone 'utc')
		);`,

		`CREATE TABLE "wallet_balances" (
			id TEXT PRIMARY KEY,
			asset TEXT,
			mint_addr TEXT,
			total TEXT,
			created_at  timestamp without time zone default (now() at time zone 'utc'),
			UNIQUE(mint_addr, total)
		);`,
	}

	down := []string{
		`DROP TABLE markets`,
		`DROP TABLE orders`,
		`DROP TABLE settles`,
		`DROP TABLE prices`,
		`DROP TABLE wallet_balances`,
	}

	addMigration("000", up, down)
}
