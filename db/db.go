package db

import (
	"database/sql"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	// Postgres database
	_ "github.com/lib/pq"
)

// New creates a new database connections
func New(auth auth) *gorm.DB {
	psqlInfo := auth.databaseSourceName()

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	return gormDB
}

func SqlDBFromGorm(gormDB *gorm.DB) *sql.DB {
	db, _ := gormDB.DB()
	return db
}
