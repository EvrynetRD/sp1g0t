package repository

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type PricesRepository struct {
	db *gorm.DB
}

func NewPricesRepository(db *gorm.DB) PricesRepository {
	return PricesRepository{db: db}
}

type AssetPrice struct {
	ID        uuid.UUID `json:"id"`
	ClientID  string    `json:"client_id"`
	Asset     string    `json:"asset"`
	Price     string    `json:"price"`
	CreatedAt time.Time `json:"created_at" gorm:"type:datetime"`
}

func (AssetPrice) TableName() string {
	return "prices"
}

func (p PricesRepository) SaveAssetPrice(assetPrice *AssetPrice) error {
	assetPrice.CreatedAt = time.Now().UTC()
	return p.db.Create(&assetPrice).Error
}

//GetMinPriceForPeriod returns the MIN price between current timestamp and volatility_period
func (p PricesRepository) GetMinPriceForPeriod(asset string, volatilityPeriod time.Duration, dateNow time.Time) (price string, err error) {
	from := dateNow.Add(-volatilityPeriod)
	err = p.db.Table("prices").Select("coalesce(min(price), '0')").
		Where("asset = ? AND created_at BETWEEN ? AND ?", asset, from, dateNow).
		Find(&price).
		Error
	return
}
