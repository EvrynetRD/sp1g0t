package repository

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type SettlesRepository struct {
	db *gorm.DB
}

func NewSettlesRepository(db *gorm.DB) SettlesRepository {
	return SettlesRepository{db: db}
}

type Settle struct {
	ID            uuid.UUID `json:"id"`
	ClientID      string    `json:"client_id"`
	Market        string    `json:"market"`
	MarketAddress string    `json:"market_address"`
	TxID          string    `json:"tx_id"`
	TxType        string    `json:"tx_type"`
	CreatedAt     time.Time `json:"created_at"`
	ModifiedAt    time.Time `json:"modified_at,omitempty"`
}

func (s SettlesRepository) SaveSettle(settle Settle) error {
	return s.db.Create(&settle).Error
}
