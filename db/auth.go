package db

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	// Postgres database
	_ "github.com/lib/pq"
)

const defaultPort = 5432

type auth struct {
	Username    string
	Password    string
	Port        int
	Hostname    string
	DatbaseName string
	UseSSL      bool
}

// GetDBAuthFromEnv grabs a database auth configuration from the environment variables
func GetDBAuthFromEnv() auth {
	host := os.Getenv("DB_HOST")
	port := defaultPort
	if strings.HasPrefix(host, "/") {
		port = 0 // using file socket
	} else {
		if ePort, err := strconv.Atoi(os.Getenv("DB_PORT")); err == nil {
			port = ePort
		}
		if host == "" {
			host = "localhost"
		}
	}

	var useSsl bool
	if eUseSsl, err := strconv.ParseBool(os.Getenv("DB_SSL")); err == nil {
		useSsl = eUseSsl
	} else {
		useSsl = true
	}

	return auth{
		Username:    os.Getenv("DB_USER"),
		Password:    os.Getenv("DB_PASSWORD"),
		Port:        port,
		DatbaseName: os.Getenv("DB_NAME"),
		Hostname:    host,
		UseSSL:      useSsl,
	}
}

// DatabaseSourceName generates the string necessary to connect to the database
func (a auth) databaseSourceName() (psqlInfo string) {
	psqlInfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s",
		a.Hostname, a.Username, a.Password, a.DatbaseName)
	if a.Port != 0 {
		psqlInfo += fmt.Sprintf(" port=%d", a.Port)
	}
	if !a.UseSSL {
		psqlInfo += " sslmode=disable"
	}
	return psqlInfo
}
