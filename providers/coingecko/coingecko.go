package coingecko

import (
	"fmt"
	"sort"
	"strings"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

type Coingecko struct {
	api CoingeckoApiInterface
}

func NewCoingecko(api CoingeckoApiInterface) *Coingecko {
	return &Coingecko{
		api: api,
	}
}

const usd = "usd"

func (c *Coingecko) GetUsdPrice(asset string) (decimal.Decimal, error) {
	list, err := c.api.getCoinsList()
	if err != nil {
		logrus.Errorf("Error while getCoinsList for asset %s", asset)
		return decimal.Decimal{}, err
	}

	var tokenIDs []string
	for _, v := range list {
		//currency names are lowercased in gecko api
		if v.Symbol == strings.ToLower(asset) {
			tokenIDs = append(tokenIDs, v.ID)
		}
	}

	//coingecko returns deffierent ids with same symbol, e.g. for ETH - ethereum-wormhole,ethereum, for BNB - binance-coin-wormhole, binancecoin,oec-binance-coin
	//and we need the shortest one as the most suitable
	shortestCurrencyID := getShortestID(tokenIDs)

	if shortestCurrencyID != "" {
		price, err := c.api.getCoinUsdPrice(shortestCurrencyID)
		if err != nil {
			logrus.Errorf("Error while getCoinUsdPrice for asset %s", asset)
			return decimal.Decimal{}, err
		}
		return price, nil
	}

	return decimal.Decimal{}, fmt.Errorf("can`t get coingecko prise for %s", asset)
}

func getShortestID(tokenIDs []string) string {
	if len(tokenIDs) != 0 {
		sort.Slice(tokenIDs, func(i, j int) bool {
			return len(tokenIDs[i]) < len(tokenIDs[j])
		})

		return tokenIDs[0]
	}
	return ""
}
