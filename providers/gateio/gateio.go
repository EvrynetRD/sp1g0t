package gateio

import (
	"fmt"
	"strings"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

type GateIO struct {
	api GateeIOApiInterface
}

func NewGateIO(api GateeIOApiInterface) *GateIO {
	return &GateIO{
		api: api,
	}
}

const usd = "USD"

func (g *GateIO) GetUsdPrice(asset string) (decimal.Decimal, error) {
	pair := fmt.Sprintf("%s_%s", strings.ToUpper(asset), usd)
	currencyPair, err := g.api.getCurrencyPair(pair)
	if err != nil {
		logrus.Errorf("Error while getCurrencyPair for asset %s", asset)
		return decimal.Decimal{}, err
	}

	price, err := decimal.NewFromString(currencyPair[0].Last)
	if err != nil {
		logrus.Errorf("Error while parsing asset price for asset %s", asset)
		return decimal.Decimal{}, err
	}
	return price, nil
}
