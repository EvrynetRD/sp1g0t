package aws

import (
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
	"github.com/sirupsen/logrus"
)

var utf8 string = "UTF-8"

const AWS_REGION = "us-west-2"

// SES is a wrapper around an initialized SES client.
type SES struct {
	service *ses.SES
}

// NewSes creates a new SES client for the region
func NewSes() SES {
	return NewSesWithRegion(AWS_REGION)
}

// NewSesWithRegion creates a new SES client for the specified AWS region
func NewSesWithRegion(region string) SES {
	cfg := aws.NewConfig().WithRegion(region)
	sess := session.Must(session.NewSession(cfg))
	svc := ses.New(sess)
	return SES{svc}
}

// SendEmail sends an email via SES.
// to may contain a comma-delimited list of email addresses
// from is the email address the message should be sent from
// html indicates whether the body is plaintext or HTML
func (s SES) SendEmail(to, from, subject, body string, html bool) error {
	var sesBody ses.Body
	if html {
		sesBody = ses.Body{
			Html: &ses.Content{
				Charset: &utf8,
				Data:    &body,
			},
		}
	} else {
		sesBody = ses.Body{
			Text: &ses.Content{
				Charset: &utf8,
				Data:    &body,
			},
		}
	}

	recipients := parseTo(to)

	_, err := s.service.SendEmail(&ses.SendEmailInput{
		Destination: &ses.Destination{ToAddresses: recipients},
		Source:      &from,
		Message: &ses.Message{
			Subject: &ses.Content{
				Charset: &utf8,
				Data:    &subject,
			},
			Body: &sesBody,
		},
	})

	if err != nil {
		logrus.Errorf("Failed to send email: %s", err)
		return err
	}
	return nil
}

// Parse a comma-delimited list of email addresses into a slice of strings,
// one per address
func parseTo(to string) []*string {
	var result []*string
	for _, s := range strings.Split(to, ",") {
		if s != "" {
			val := s
			result = append(result, &val)
		}
	}
	return result
}
