package hydra

import (
	"errors"

	"github.com/stretchr/testify/mock"
	"gitlab.com/EvrynetRD/sp1g0t/models"
)

type HydraMock struct {
	mock.Mock
}

func (hm *HydraMock) Register(req models.RegistrationReq) (err error) {
	return errors.New("not implemented in mock")
}

func (hm *HydraMock) Verify(token string) (err error) {
	return errors.New("not implemented in mock")
}

func (hm *HydraMock) GetMarkets(basep, quotep string) (marketPairs models.MarketPairs, err error) {
	return models.MarketPairs{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) GetMarketPairData(pair string) (marketResult models.MarketResult, err error) {
	return models.MarketResult{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) GetOrders() (orders []models.OrderResult, err error) {
	return nil, errors.New("not implemented in mock")
}
func (hm *HydraMock) GetOrdersForMarket(marketAddress string) (orders models.OrderResult, err error) {
	return models.OrderResult{
		Orders: []models.OrderData{
			{
				Id:            "id1",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "1050",
				Side:          "sell",
				Size:          "2",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id2",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "800",
				Side:          "sell",
				Size:          "2",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id3",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "1070",
				Side:          "sell",
				Size:          "3",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id11",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "990",
				Side:          "buy",
				Size:          "5",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id22",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "800",
				Side:          "buy",
				Size:          "20",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id4",
				ClientId:      "client1",
				MarketAddress: "address1",
				Price:         "4",
				Side:          "sell",
				Size:          "2",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id5",
				ClientId:      "client1",
				MarketAddress: "address1",
				Price:         "3",
				Side:          "sell",
				Size:          "1",
				Market:        "BTC/USDT",
			},
		},
	}, nil
}
func (hm *HydraMock) GetOrder(id string) (order models.OrderResult, err error) {
	return models.OrderResult{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) PlaceOrder(req models.OrderReq) (tx models.SignatureReq, err error) {
	return models.SignatureReq{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) CancelAllOrders() (tx models.SignatureReq, err error) {
	return models.SignatureReq{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) CancelOrder(id string) (tx models.SignatureReq, err error) {
	return models.SignatureReq{
		TxId:   "txid",
		TxData: "txdata",
	}, nil
}
func (hm *HydraMock) Sign(txID string, signatureData models.SignatureData) (res models.SignedOrders, err error) {
	return models.SignedOrders{
		Result: models.Result{
			Success: true,
		},
	}, nil
}

func (hm *HydraMock) SignBody(payload string) string {
	return "not implemented"
}

func (hm *HydraMock) Settle(market, marketAddress string) (tx models.SignatureReq, err error) {
	return models.SignatureReq{
		TxId:   "txid",
		TxData: "txdata",
	}, nil
}

func (hm *HydraMock) GetWalletBalances() (balances *models.WalletBalancesResult, err error) {
	return &models.WalletBalancesResult{
		Success: true,
		Balances: []models.BalanceData{
			{
				Asset:    "RAY",
				MintAddr: "some_mint_addr",
				Total:    "100",
			},
			{
				Asset:    "BTC",
				MintAddr: "some_mint_addr2",
				Total:    "999",
			},
		},
	}, nil
}

func (hm *HydraMock) GetMarketBalances(marketAddress string) (balances *models.OpenBalancesResult, err error) {
	return nil, errors.New("not implemented in mock")
}

var _ HydraInterface = (*HydraMock)(nil)
