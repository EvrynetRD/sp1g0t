package hydra

import (
	"bytes"
	"crypto/ed25519"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/btcsuite/btcutil/base58"
	"github.com/sirupsen/logrus"
)

func (h *Hydra) sign(payload string) string {
	decodedKey := base58.Decode(h.privateKey)
	private := ed25519.PrivateKey(decodedKey)
	sig := ed25519.Sign(private, []byte(payload))
	return hex.EncodeToString(sig)
}

func (h *Hydra) SignBody(payload string) string {
	decodedKey := base58.Decode(h.privateKey)
	private := ed25519.PrivateKey(decodedKey)
	payloadDecoded, err := base64.StdEncoding.DecodeString(payload)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	sig := ed25519.Sign(private, payloadDecoded)
	return hex.EncodeToString(sig)
}

func (h *Hydra) signReq(path string, method string, body []byte) (*http.Request, error) {
	ts := strconv.FormatInt(time.Now().UnixMilli(), 10)

	signaturePayload := ts + method + removeQueryParamsForSign(path) + string(body)
	signature := h.sign(signaturePayload)

	req, err := http.NewRequest(method, h.host+path, bytes.NewBuffer(body))
	if err != nil {
		logrus.Errorf("[%s] while creating request to %s", err, path)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-FG-KEY", h.publicKey)
	req.Header.Set("X-FG-SIGN", signature)
	req.Header.Set("X-FG-TS", ts)

	return req, nil
}

//for signing we dont need `path` without any query params (like ?smth=smth_else)
func removeQueryParamsForSign(path string) string {
	if idx := strings.Index(path, "?"); idx != -1 {
		return path[:idx]
	}
	return path
}
