package utils

import (
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

func Test_ParseStringToTime(t *testing.T) {
	timeStr := "2022-02-22T14:00"
	res, err := ParseStringToTime(timeStr)
	assert.Nil(t, err)
	assert.False(t, res.IsZero())
	assert.True(t, res.Year() == 2022)
	assert.True(t, res.Month() == 2)
	assert.True(t, res.Day() == 22)
	assert.True(t, res.Hour() == 14)
	assert.True(t, res.Minute() == 0)
}

func Test_PercentageDeviation(t *testing.T) {
	price := PercentageDeviation(decimal.NewFromInt(80), decimal.NewFromInt(100))
	assert.True(t, decimal.NewFromInt(25).Equal(price))

	price = PercentageDeviation(decimal.NewFromInt(100), decimal.NewFromInt(90))
	assert.True(t, decimal.NewFromInt(10).Equal(price))
}

func Test_FindTheClosestBiggestWithoutReminder(t *testing.T) {
	closest := FindTheClosestBiggestWithoutReminder(decimal.NewFromFloat(1.3), decimal.NewFromFloat(0.2))
	assert.True(t, closest.Equal(decimal.NewFromFloat(1.4)))

	closest = FindTheClosestBiggestWithoutReminder(decimal.NewFromFloat(2.7), decimal.NewFromFloat(0.3))
	assert.True(t, closest.Equal(decimal.NewFromFloat(3)))

	closest = FindTheClosestBiggestWithoutReminder(decimal.NewFromFloat(2.3), decimal.NewFromFloat(0.8))
	assert.True(t, closest.Equal(decimal.NewFromFloat(2.4)))
}
