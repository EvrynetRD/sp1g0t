package utils

import (
	"github.com/badoux/checkmail"
)

//TODO: probably we need SMTP server to check reverse validation
//https://github.com/badoux/checkmail#3-host-and-user
func ValidateEmail(email string) (err error) {
	err = checkmail.ValidateFormat(email)
	if err != nil {
		return
	}
	err = checkmail.ValidateHost(email)
	if err != nil {
		return
	}
	return
}
