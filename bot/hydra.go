package bot

import (
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/EvrynetRD/sp1g0t/models"
)

func (b Sp1g0tBot) Register(req models.RegistrationReq) error {
	return b.hydraAPI.Register(req)
}

func (b Sp1g0tBot) Verify(token string) error {
	return b.hydraAPI.Verify(token)
}

func (b Sp1g0tBot) GetMarkets(basep, quotep string) ([]models.MarketData, error) {
	marketPairs, err := b.GetMarketPairs(basep, quotep)
	if err != nil {
		return nil, err
	}

	var markets []models.MarketData
	for _, pair := range marketPairs {
		marketData, err := b.GetMarketPairData(pair)
		if err != nil {
			logrus.Warnf("failed to find market data for %s pair", pair)
			continue
		}
		markets = append(markets, marketData.Market)
	}
	return markets, nil
}

func (b Sp1g0tBot) GetMarketPairs(basep, quotep string) ([]string, error) {
	marketPairs, err := b.hydraAPI.GetMarkets(basep, quotep)
	if err != nil {
		return nil, err
	}
	return marketPairs.Pairs, nil
}

func (b Sp1g0tBot) GetMarketPairData(pair string) (marketResult models.MarketResult, err error) {
	return b.hydraAPI.GetMarketPairData(strings.Replace(pair, "/", "", -1))
}

func (b Sp1g0tBot) GetOrder(id string, isLocal bool) (order models.OrderResult, err error) {
	if isLocal {
		return b.GetLocalOrder(id)
	}
	return b.hydraAPI.GetOrder(id)
}

func (b Sp1g0tBot) GetOrdersForMarket(marketAddress string) (orders models.OrderResult, err error) {
	return b.hydraAPI.GetOrdersForMarket(marketAddress)
}

func (b Sp1g0tBot) PlaceOrder(clientID, market, marketAddress, price, side, size, orderType string) (*models.SignedOrders, error) {
	req := models.OrderReq{
		ClientId:      clientID,
		Market:        market,
		MarketAddress: marketAddress,
		Price:         price,
		Side:          side,
		Size:          size,
		OrderType:     orderType,
	}
	tx, err := b.hydraAPI.PlaceOrder(req)
	if err != nil {
		return nil, err
	}

	signedOrder, err := b.sign(tx.TxId, tx.TxData)
	if err != nil {
		return nil, err
	}

	err = b.SaveOrderToLocal(signedOrder.Orders, tx)
	if err != nil {
		return nil, err
	}

	return signedOrder, nil
}

func (b Sp1g0tBot) CancelOrder(id string) (*models.SignedOrders, error) {
	tx, err := b.hydraAPI.CancelOrder(id)
	if err != nil {
		return nil, err
	}
	canceledOrder, err := b.sign(tx.TxId, tx.TxData)
	if err != nil {
		return nil, err
	}

	err = b.CancelLocalOrder(id, tx.TxId)
	if err != nil {
		return nil, err
	}

	return canceledOrder, nil
}

func (b Sp1g0tBot) CancelOrdersByStatus(status string) error {
	orders, err := b.GetOrdersByStatus(strings.ToUpper(status))
	if err != nil {
		return err
	}
	for _, order := range orders {
		_, err := b.CancelOrder(order.Id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (b Sp1g0tBot) Settle(market, marketAddress string) error {
	tx, err := b.hydraAPI.Settle(market, marketAddress)
	if err != nil {
		return err
	}

	err = b.SaveSettleToLocal(market, marketAddress, tx)
	if err != nil {
		return err
	}

	_, err = b.sign(tx.TxId, tx.TxData)
	if err != nil {
		return err
	}

	return nil
}

func (b Sp1g0tBot) sign(txID, txData string) (*models.SignedOrders, error) {
	body := b.hydraAPI.SignBody(txData)
	res, err := b.hydraAPI.Sign(txID, models.SignatureData{Signature: body})
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (b Sp1g0tBot) GetWalletBalances() (balances *models.WalletBalancesResult, err error) {
	balances, err = b.hydraAPI.GetWalletBalances()
	if err != nil {
		return nil, err
	}
	return
}

func (b Sp1g0tBot) GetMarketBalances(marketAddress string) (balances *models.OpenBalancesResult, err error) {
	return b.hydraAPI.GetMarketBalances(marketAddress)
}
