package bot

import (
	"strings"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

func (b Sp1g0tBot) GetPriceForAssetPair(assetPair string) (*decimal.Decimal, error) {
	assetPairs := strings.Split(assetPair, "/")
	base := assetPairs[0]
	quote := assetPairs[1]

	avgBasePrice, err := b.GetAvgUSDPriceFromProviders(base)
	if err != nil {
		return nil, err
	}
	logrus.Infof("average %s price is %v", base, avgBasePrice)

	avgQuotePrice, err := b.GetAvgUSDPriceFromProviders(quote)
	if err != nil {
		return nil, err
	}
	logrus.Infof("average %s price is %v", quote, avgQuotePrice)

	avgPrice := avgBasePrice.Div(avgQuotePrice)
	logrus.Infof("average price for %s is %v", assetPair, avgPrice)
	return &avgPrice, nil
}

func (b Sp1g0tBot) GetAvgUSDPriceFromProviders(asset string) (decimal.Decimal, error) {
	coingeckoPrice, err := b.PriceProvider.Coingecko.GetUsdPrice(asset)
	if err != nil {
		logrus.Error("Error while fetching coingecko price. ", err)
	}
	logrus.Infof("coingeckoPrice for %s is %v", asset, coingeckoPrice)

	gateIOPrice, err := b.PriceProvider.GateIO.GetUsdPrice(asset)
	if err != nil {
		logrus.Error("Error while fetching gateio price. ", err)
	}
	logrus.Infof("gateIOPrice for %s is %v", asset, gateIOPrice)

	return getAverage(coingeckoPrice, gateIOPrice), nil
}

// getAveragePrice calculates average for non-zero prices
func getAverage(prices ...decimal.Decimal) decimal.Decimal {
	if prices == nil {
		logrus.Error("empty prices from providers")
		return decimal.Decimal{}
	}
	var res []decimal.Decimal
	for _, p := range prices {
		if p.IsZero() {
			continue
		}
		res = append(res, p)
	}

	return decimal.Avg(res[0], res[1:]...)
}
