package bot

import (
	"github.com/shopspring/decimal"
	"gitlab.com/EvrynetRD/sp1g0t/db/repository"
	"gitlab.com/EvrynetRD/sp1g0t/models"
)

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func isOwnOrder(localOrders []repository.Order, order models.OrderData) bool {
	for _, lo := range localOrders {
		if lo.ClientId == order.ClientId {
			return true
		}
	}
	return false
}

//(number/100)*percentage
func percentageFromNumber(number, percentage decimal.Decimal) decimal.Decimal {
	return number.Div(decimal.NewFromInt(100)).Mul(percentage)
}
