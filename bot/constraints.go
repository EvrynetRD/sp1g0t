package bot

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/EvrynetRD/sp1g0t/db/repository"
	"gitlab.com/EvrynetRD/sp1g0t/models"
	"gitlab.com/EvrynetRD/sp1g0t/utils"
)

// Get Safety Margin
func (b Sp1g0tBot) getSM(marketPrice decimal.Decimal, assetPair, side string) (decimal.Decimal, error) {
	var sm decimal.Decimal

	smValue := b.BotConfig.GetSafetyMarginValue(assetPair)

	switch {
	case side == utils.SIDE_SELL:
		sm = marketPrice.Add(percentageFromNumber(marketPrice, smValue))
	case side == utils.SIDE_BUY:
		sm = marketPrice.Sub(percentageFromNumber(marketPrice, smValue))
	default:
		return decimal.Decimal{}, errors.New("wrong side")
	}
	return sm, nil
}

// Get Foreign order in price range
//isOrderInRange
// for a bid: market price > price > market  price - BM%
// for an ask: market price < price < market price + AM%
func (b Sp1g0tBot) isFOinRange(marketPrice decimal.Decimal, order models.OrderData) (bool, error) {
	bmValue := b.BotConfig.GetBidMarginValue(order.Market)
	amValue := b.BotConfig.GetAskMarginValue(order.Market)

	orderPriceDecimal, err := decimal.NewFromString(order.Price)
	if err != nil {
		logrus.Error(err)
		return false, err
	}

	switch {
	case order.Side == utils.SIDE_BUY:
		return marketPrice.GreaterThan(orderPriceDecimal) && orderPriceDecimal.GreaterThan(marketPrice.Sub(percentageFromNumber(marketPrice, bmValue))), nil
	case order.Side == utils.SIDE_SELL:
		return marketPrice.LessThan(orderPriceDecimal) && orderPriceDecimal.LessThan(marketPrice.Add(percentageFromNumber(marketPrice, amValue))), nil
	default:
		return false, errors.New("wrong side")
	}
}

// ask = sell
// bid = buy
//FAD: foreign ask depth
//FBD: foreign bid depth
func (b Sp1g0tBot) getFADandFBD(marketAddress string, marketPrice decimal.Decimal) (decimal.Decimal, decimal.Decimal, error) {
	orders, err := b.hydraAPI.GetOrdersForMarket(marketAddress)
	if err != nil {
		logrus.Error(err)
		return decimal.Decimal{}, decimal.Decimal{}, err
	}

	localOrders, err := b.GetOwnOrdersForMarket(marketAddress)
	if err != nil {
		logrus.Error(err)
		return decimal.Decimal{}, decimal.Decimal{}, err
	}

	var ordersAskDepth, ordersBidDepth decimal.Decimal

	for _, order := range orders.Orders {
		//skip own order
		if isOwnOrder(localOrders, order) {
			continue
		}

		//if order is not in range isFOinRange() => skip
		isInFoRange, err := b.isFOinRange(marketPrice, order)
		if err != nil {
			logrus.Error(err)
			return decimal.Decimal{}, decimal.Decimal{}, err
		}
		if !isInFoRange {
			continue
		}

		if order.Side == utils.SIDE_SELL {
			depth, err := getOrderDepth(order.Size, order.Price)
			if err != nil {
				logrus.Error(err)
				return decimal.Decimal{}, decimal.Decimal{}, err
			}

			ordersAskDepth = ordersAskDepth.Add(depth)
		}
		if order.Side == utils.SIDE_BUY {
			depth, err := getOrderDepth(order.Size, order.Price)
			if err != nil {
				logrus.Error(err)
				return decimal.Decimal{}, decimal.Decimal{}, err
			}

			ordersBidDepth = ordersBidDepth.Add(depth)
		}
	}

	return ordersAskDepth, ordersBidDepth, nil
}

//OBG: order book gap
func (b Sp1g0tBot) getOBG(fad, fbd decimal.Decimal) decimal.Decimal {
	return fad.Sub(fbd).Abs()
}

//Order book gap is within tolerance when (1 - (min(FAD, FBD) / max(FAD, FBD))) * 100 < OBGT%
func (b Sp1g0tBot) getOBGwithinTolerance(fad, fbd decimal.Decimal, market string) bool {
	obgtValue := b.BotConfig.GetOBGTValue(market)
	//if FAD or FBD is 0, then OBGwithinTolerance value should be 100%
	if fad.IsZero() || fbd.IsZero() {
		return decimal.NewFromInt(100).LessThan(obgtValue)
	}

	return (decimal.NewFromInt(1).
		Sub(
			(decimal.Min(fad, fbd)).
				Div(decimal.Max(fad, fbd)))).
		Mul(decimal.NewFromInt(100)).LessThan(obgtValue)
}

//OOR-OO: out of range own order
func (b Sp1g0tBot) getOOROO(smPrice, marketPrice decimal.Decimal, order repository.Order) bool {
	price, err := decimal.NewFromString(order.Price)
	if err != nil {
		return false
	}
	if !(price.GreaterThanOrEqual(decimal.Min(smPrice, marketPrice)) && price.LessThanOrEqual(decimal.Max(smPrice, marketPrice))) {
		return true
	} else {
		return false
	}
}

func (b Sp1g0tBot) checkBlackisted(marketName string) bool {
	for _, asset := range strings.Split(b.BotConfig.AssetBlackList, ",") {
		if strings.Contains(marketName, asset) {
			logrus.Infof("Market %s is blacklisted.", marketName)
			return true
		}
	}
	return false
}

func (b Sp1g0tBot) getSettlementTreshold(asset string) decimal.Decimal {
	return b.BotConfig.GetSettlementTreshold(asset)
}

func getOrderDepth(size, price string) (decimal.Decimal, error) {
	sizeDecimal, err := decimal.NewFromString(size)
	if err != nil {
		logrus.Error(err)
		return decimal.Decimal{}, err
	}
	priceDecimal, err := decimal.NewFromString(price)
	if err != nil {
		logrus.Error(err)
		return decimal.Decimal{}, err
	}
	return sizeDecimal.Mul(priceDecimal), nil
}

func (b Sp1g0tBot) checkSettlementTreshold(marketAddress, asset string, marketPrice decimal.Decimal) error {
	settlementThreshold := b.getSettlementTreshold(asset)
	if marketPrice.GreaterThan(settlementThreshold) {
		err := b.Settle(asset, marketAddress)
		if err != nil {
			return err
		}
	}
	return nil
}

//checkWalletBalance check if balance for given asset is bigger then MIN_BALANCE constraint
//returns `false` in case of any error and when MIN_BALANCE > asset amount
// returns true if MIN_BALANCE < asset amount
func (b Sp1g0tBot) checkWalletBalance(assetPair, mintAddress string, balances []models.BalanceData) (bool, error) {
	assets := strings.Split(assetPair, "/")
	base := assets[0]

	minBalance := b.BotConfig.GetMinBalance(base)

	for _, balance := range balances {
		if balance.MintAddr == mintAddress {
			totalAmount, err := decimal.NewFromString(balance.Total)
			if err != nil {
				return false, err
			}
			if minBalance.GreaterThan(totalAmount) {
				return false, nil
			}
			return true, nil
		}
	}
	return false, nil
}

func (b Sp1g0tBot) getDoNotTradePeriod(now time.Time) (time.Duration, bool) {
	for _, t := range b.BotConfig.DoNotTradePeriods {
		if now.After(t.From) && now.Before(t.To) {
			notTradeDuration := t.To.Sub(now)
			return notTradeDuration, true
		}
		continue
	}
	return 0, false
}

// checkVolatilityTreshold check that deviation shouldnt be more then volatility_treshold
// if deviation > volatility_treshold return true, means not volatile and we will stop trading
// if deviation < volatility_treshold return false and means that we can continue trading
func (b Sp1g0tBot) checkVolatilityTreshold(marketPrice decimal.Decimal, assetPair string, dateNow time.Time) bool {
	volatilityThreshold := b.BotConfig.GetVolatilityThreshold(assetPair)
	volatilityPeriod := b.BotConfig.GetVolatilityPeriod(assetPair)

	price, err := b.GetMinPriceForPeriod(assetPair, volatilityPeriod, dateNow)
	if err != nil {
		logrus.Error(err)
		return false
	}

	priceDecimal, err := decimal.NewFromString(price)
	if err != nil {
		logrus.Error(err)
		return false
	}

	deviation := utils.PercentageDeviation(marketPrice, priceDecimal)
	fmt.Println(deviation)
	return !deviation.GreaterThan(volatilityThreshold)
}

func (b Sp1g0tBot) calculateWalletValue(balances []models.BalanceData, markets []models.MarketData) decimal.Decimal {
	var totalInUSD decimal.Decimal
	for _, balance := range balances {
		for _, market := range markets {
			if market.BaseMintAddr == balance.MintAddr {
				avgUSDPrice, err := b.GetAvgUSDPriceFromProviders(market.Base)
				if err != nil {
					logrus.Errorf("cannot calculate usd price for %s", market.Base)
					return decimal.Decimal{}
				}
				quantityDecimal, err := decimal.NewFromString(balance.Total)
				if err != nil {
					logrus.Errorf("cannot calculate quantityDecimal for %s", market.Base)
					return decimal.Decimal{}
				}
				totalInUSD = totalInUSD.Add(avgUSDPrice.Mul(quantityDecimal))
			}
		}
	}
	return totalInUSD
}

func (b Sp1g0tBot) checkLossTreshold(currentBalance, oldBalance decimal.Decimal) bool {
	balanceDiff := currentBalance.Sub(oldBalance)

	return currentBalance.GreaterThanOrEqual(oldBalance) || balanceDiff.GreaterThan(b.BotConfig.LossThreshold)
}

func (b Sp1g0tBot) getOrderSize(obg, maxOrderSize decimal.Decimal, minOrderSize string) decimal.Decimal {
	minOrderSizeDecimal, err := decimal.NewFromString(minOrderSize)
	if err != nil {
		return decimal.Decimal{}
	}

	orderSize := decimal.Min(obg, maxOrderSize)

	if orderSize.LessThan(minOrderSizeDecimal) {
		return minOrderSizeDecimal
	}

	return orderSize
}
