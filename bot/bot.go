package bot

import (
	"encoding/csv"
	"os"
	"sort"

	"github.com/dnlo/struct2csv"
	"github.com/sirupsen/logrus"
	"gitlab.com/EvrynetRD/sp1g0t/aws"
	"gitlab.com/EvrynetRD/sp1g0t/config"
	"gitlab.com/EvrynetRD/sp1g0t/db"
	"gitlab.com/EvrynetRD/sp1g0t/db/repository"
	"gitlab.com/EvrynetRD/sp1g0t/hydra"
	"gitlab.com/EvrynetRD/sp1g0t/providers"
	"gitlab.com/EvrynetRD/sp1g0t/utils"
)

type Sp1g0tBot struct {
	hydraAPI      hydra.HydraInterface
	Migrator      *db.Migration
	Repository    repository.Repository
	BotConfig     *config.Sp1g0tConfig
	PriceProvider *providers.PriceProvider
	AwsSES        aws.SES
}

func NewSp1g0tBot(hydraAPI hydra.HydraInterface, migrator *db.Migration, repository repository.Repository,
	priceProvider *providers.PriceProvider, awsSES aws.SES) Sp1g0tBot {
	return Sp1g0tBot{
		hydraAPI:      hydraAPI,
		Migrator:      migrator,
		Repository:    repository,
		PriceProvider: priceProvider,
		AwsSES:        awsSES,
	}
}

//GetTokensList list all tokens (from the set of asset pairs supported by the API server) in alphabetical order
func (b Sp1g0tBot) GetTokensList() (tokenList []string, err error) {
	pairs, err := b.GetMarketPairs("", "")
	if err != nil {
		return
	}
	for _, pair := range pairs {
		if !contains(tokenList, pair) {
			tokenList = append(tokenList, pair)
		}
	}

	sort.Strings(tokenList)

	return
}

func (b Sp1g0tBot) MarketsToCSV(fileName string) (err error) {
	markets, err := b.GetMarkets("", "")
	if err != nil {
		return
	}

	enc := struct2csv.New()
	rows, err := enc.Marshal(markets)
	if err != nil {
		return
	}

	csvFile, err := os.Create(fileName)
	if err != nil {
		return
	}

	defer func() {
		if err := csvFile.Close(); err != nil {
			logrus.Errorf("Error closing file: %s\n", err)
		}
	}()

	csvwriter := csv.NewWriter(csvFile)

	err = csvwriter.WriteAll(rows)
	if err != nil {
		return
	}
	return
}

func (b Sp1g0tBot) ShowCanceledOrders() error {
	orders, err := b.Repository.Order.GetOrdersByStatus(utils.ORDER_STATUS_CANCELED)
	if err != nil {
		return err
	}

	enc := struct2csv.New()
	rows, err := enc.Marshal(orders)
	if err == struct2csv.ErrNilSlice || err == struct2csv.ErrEmptySlice {
		logrus.Warn("There are no canceled orders.")
		return nil
	} else if err != nil {
		return err
	}

	writer := csv.NewWriter(os.Stdout)
	for _, row := range rows {
		err := writer.Write(row)
		if err != nil {
			return err
		}
	}
	if writer.Error() != nil {
		return err
	}
	writer.Flush()

	return nil
}
