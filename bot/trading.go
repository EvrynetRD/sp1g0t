package bot

import (
	"fmt"
	"sync"
	"time"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/EvrynetRD/sp1g0t/db/repository"
	"gitlab.com/EvrynetRD/sp1g0t/models"
	"gitlab.com/EvrynetRD/sp1g0t/utils"
)

var wg sync.WaitGroup

func (b Sp1g0tBot) Start() {
	b.startTrading(time.Now().UTC())
	// for {
	// 	<-time.After(5 * time.Second)
	// 	wg.Add(1)
	// 	go func() {
	// 		defer wg.Done()
	// 		b.startTrading(time.Now().UTC())
	// 	}()
	// 	wg.Wait()
	// }
}

func (b Sp1g0tBot) startTrading(now time.Time) {
	// the bot shall cancel all open orders before entering the "no trade" period.
	b.checkDoNotTradePeriod(now)

	logrus.Info("Getting info about balances...")
	balances, err := b.GetWalletBalances()
	if err != nil {
		logrus.Error(err)
		return
	}

	//filter markets to trade only on markets where we have balances
	marketsForTrading := b.filterMarketsForTrading(balances.Balances)
	if marketsForTrading == nil {
		logrus.Warn("No markets for trading...")
		return
	}

	isPortfoliOk := b.checkPortfolio(now, marketsForTrading, balances.Balances)
	if !isPortfoliOk {
		logrus.Warnf("Portfolio with losses. Pause for %+vs", b.BotConfig.LossPeriod)
		//should take a break for LOSS_PERIOD and pause trading
		//cancel all own order in all markets
		err := b.cancelOwnOpenOrders()
		if err != nil {
			logrus.Error(err)
		}
		time.Sleep(time.Duration(b.BotConfig.LossPeriod) * time.Second)
		b.startTrading(time.Now().UTC())
	}

	for _, m := range marketsForTrading {
		//skip blacklisted markets
		// xxx/BTC and BTC/xxx
		if b.checkBlackisted(m.Name) {
			continue
		}

		marketPrice, err := b.GetPriceForAssetPair(m.Name)
		if err != nil {
			continue
		}

		err = b.saveMarketPrice(m.Name, *marketPrice)
		if err != nil {
			continue
		}

		if !b.checkVolatilityTreshold(*marketPrice, m.Name, now) {
			//cancel open orders and stop trading
			err := b.cancelAllOwnOrders(m.MarketAddress)
			if err != nil {
				logrus.Error(err)
				continue
			}
			//stop trading -> go to next market
			continue
		}

		err = b.checkSettlementTreshold(m.MarketAddress, m.Name, *marketPrice)
		if err != nil {
			logrus.Error(err)
			continue
		}

		//1. cancel own orders which are out of range
		err = b.cancelOwnOutOfRangeOrders(m.MarketAddress, *marketPrice)
		if err != nil {
			//if there is an issue with canceling own orders we continue execution and log the error
			logrus.Error(err)
		}

		//2.compute the FAD/FBD for asset pair
		fad, fbd, err := b.getFADandFBD(m.MarketAddress, *marketPrice)
		if err != nil {
			logrus.Error(err)
			continue
		}
		//if both are 0 then switch to another market
		if fad.IsZero() && fbd.IsZero() {
			continue
		}

		//3. OBG within tolerance?
		isWithinTolerance := b.getOBGwithinTolerance(fad, fbd, m.Name)
		if isWithinTolerance {
			continue
		}

		isEnoughBalance, err := b.checkWalletBalance(m.Name, m.BaseMintAddr, balances.Balances)
		if err != nil {
			logrus.Error(err)
			continue
		}
		if !isEnoughBalance {
			logrus.Warnf("not enough balance for %s", m.Name)
			//uncomment when amazon SES would be active
			// err := b.AwsSES.SendEmail(b.BotConfig.Email, "from@email.com", "Wallet balance", "Not enough balance", false)
			// if err != nil {
			// 	logrus.Error(err)
			// }
			continue
		}

		orderSize := b.getOrderSize(b.getOBG(fad, fbd), b.BotConfig.GetMaxOrderSize(m.Name), m.MinOrderSize)
		if fad.GreaterThan(fbd) {
			//6. reduce OAD and increase OBD until equilibrium is reached (place buy orders)
			err = b.equalizeEquilibrium(m.Name, m.MarketAddress, utils.SIDE_BUY, m.TickSize, orderSize, *marketPrice)
			if err != nil {
				logrus.Error(err)
			}
		} else {
			//7.reduce OBD and increase OAD until equilibrium is reached (place sell orders)
			err = b.equalizeEquilibrium(m.Name, m.MarketAddress, utils.SIDE_SELL, m.TickSize, orderSize, *marketPrice)
			if err != nil {
				logrus.Error(err)
			}
		}

		logrus.Infof("Market %s is done, going to next one", m.Name)
	}
}

func (b Sp1g0tBot) checkDoNotTradePeriod(now time.Time) {
	notTradeDuration, isDoNotTradePeriod := b.getDoNotTradePeriod(now)
	if isDoNotTradePeriod {
		b.cancelOwnOpenOrders()
		logrus.Warnf("Gonna freeze for %s because of not_to_trade_period", notTradeDuration)
		time.Sleep(notTradeDuration)
		logrus.Info("wake up, Neo, continue execution")
		return
	}
}

//cancelOwnOpenOrders cancel all own orders with Open status from all markets
func (b Sp1g0tBot) cancelOwnOpenOrders() error {
	ownOrders, err := b.GetOrdersByStatus(utils.ORDER_STATUS_OPEN)
	if err != nil {
		logrus.Error(err)
		return err
	}
	for _, order := range ownOrders {
		_, err := b.CancelOrder(order.Id)
		if err != nil {
			logrus.Error(err)
			continue
		}
	}
	return nil
}

//cancelOwnOrders check orders from local db (own orders) and cancel them returning  ids
func (b Sp1g0tBot) cancelOwnOutOfRangeOrders(marketAddress string, marketPrice decimal.Decimal) error {
	ownOrders, err := b.GetOwnOrdersForMarket(marketAddress)
	if err != nil {
		return err
	}

	for _, order := range ownOrders {
		orderSMprice, err := b.getSM(marketPrice, order.Market, order.Side)
		if err != nil {
			continue
		}

		isOROO := b.getOOROO(orderSMprice, marketPrice, order)
		if isOROO {
			_, err := b.CancelOrder(order.Id)
			if err != nil {
				continue
			}
		}
	}
	return nil
}

//cancelAllOwnOrders cancel all own orders for given market
func (b Sp1g0tBot) cancelAllOwnOrders(marketAddress string) error {
	ownOrders, err := b.GetOwnOrdersForMarket(marketAddress)
	if err != nil {
		return err
	}
	for _, order := range ownOrders {
		_, err := b.CancelOrder(order.Id)
		if err != nil {
			logrus.Warnf("cannot cancel order with id %s. Continue..", order.Id)
			continue
		}
	}
	return nil
}

func (b Sp1g0tBot) equalizeEquilibrium(marketName, marketAddress, side, tickSize string, orderSize, marketPrice decimal.Decimal) error {
	orderPrice, err := b.getSM(marketPrice, marketName, side)
	if err != nil {
		return err
	}
	tickSizeDecimal, err := decimal.NewFromString(tickSize)
	if err != nil {
		return err
	}

	if side == utils.SIDE_BUY {
		marketPrice = marketPrice.Sub(marketPrice.Mod(tickSizeDecimal))
	} else if side == utils.SIDE_SELL {
		marketPrice = utils.FindTheClosestBiggestWithoutReminder(marketPrice, tickSizeDecimal)
	}

	_, err = b.PlaceOrder(b.BotConfig.WalletAddress, marketName, marketAddress, orderPrice.String(), side, orderSize.String(), "limit")
	if err != nil {
		return err
	}
	return nil
}

func (b Sp1g0tBot) saveMarketPrice(asset string, marketPrice decimal.Decimal) error {
	err := b.SaveAssetPrice(asset, marketPrice)
	if err != nil {
		logrus.Error("Error while saving price to db", err)
		return err
	}
	return nil
}

func (b Sp1g0tBot) getWalletBalancesForLossPeriod(dateNow time.Time) ([]repository.WalletBalance, error) {
	lossPeriod := b.BotConfig.GetLossPeriod()
	walletBalances, err := b.GetWalletBalancesForPeriod(lossPeriod, dateNow)
	if err != nil {
		logrus.Error("Error while GetWalletBalancesForPeriod", err)
		return nil, err
	}

	return walletBalances, nil
}

func (b Sp1g0tBot) checkPortfolio(now time.Time, markets []models.MarketData, currentBalances []models.BalanceData) bool {
	localBalances, err := b.getWalletBalancesForLossPeriod(now)
	if err != nil {
		return false
	}

	if localBalances == nil {
		err := b.SaveWalletBalancesToLocal(currentBalances, now)
		if err != nil {
			return false
		}
	} else {
		diff := now.Sub(localBalances[0].CreatedAt).Seconds()
		if diff >= b.BotConfig.LossPeriod {
			currentPortfolio := b.calculateWalletValue(currentBalances, markets)

			var localBalancesToBalances []models.BalanceData
			for _, b := range localBalances {
				localBalancesToBalances = append(localBalancesToBalances, repoWalletBalanceToBalanceData(b))
			}
			previosPortfolio := b.calculateWalletValue(localBalancesToBalances, markets)

			fmt.Println(currentPortfolio, previosPortfolio)
			return b.checkLossTreshold(currentPortfolio, previosPortfolio)
		}
	}
	return true
}

func (b Sp1g0tBot) filterMarketsForTrading(balances []models.BalanceData) (marketsForTrading []models.MarketData) {
	markets, err := b.GetMarkets("", "")
	if err != nil {
		logrus.Error(err)
		return nil
	}
	for _, balance := range balances {
		for _, market := range markets {
			if balance.MintAddr == market.BaseMintAddr {
				marketsForTrading = append(marketsForTrading, market)
			}
		}
	}
	return
}
