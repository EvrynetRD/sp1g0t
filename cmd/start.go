package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/EvrynetRD/sp1g0t/bot"
	"gitlab.com/EvrynetRD/sp1g0t/config"
	"gitlab.com/EvrynetRD/sp1g0t/utils"
)

func newStartCommand(b bot.Sp1g0tBot) *cobra.Command {
	// startCmd represents the start command
	startCmd := &cobra.Command{
		Use:   "start",
		Short: "Starts trading using saved configs",
		Long:  `Starts trading using saved configs`,
		RunE:  executeStartCommand(b),
	}
	return startCmd
}

// initConfigs read data from .env and build cfg object for bot execution
func initConfigs(b bot.Sp1g0tBot) (*config.Sp1g0tConfig, error) {
	markets, err := b.GetMarketPairs("", "")
	if err != nil {
		logrus.Errorf("error getting markets to check configs")
		return nil, err
	}

	cfg, err := config.InitConfigs(markets)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func executeStartCommand(b bot.Sp1g0tBot) utils.RunEFunc {
	return func(cmd *cobra.Command, args []string) error {
		logrus.Info("Getting configurations ... ")
		botConfig, err := initConfigs(b)
		if err != nil {
			logrus.Errorf("Cannot read from configuration file. Error = %s. Please check your env variables file", err)
			return err
		}
		logrus.Info("DONE")

		logrus.Info("Starting bot ... ")

		b.BotConfig = botConfig

		b.Start()

		return nil
	}
}
