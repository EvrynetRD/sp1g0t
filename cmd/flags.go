package cmd

var GlobalFlags struct {
	TokensList  bool
	MarketsList bool
	Basep       string
	Quotep      string
	ToCSV       string
	Register    bool
	Verify      bool
	Code        string
}

var MigrateFlags struct {
	Up      bool
	Down    bool
	Version *uint
}

var OrderFlags struct {
	Place   bool
	Get     bool
	OrderID string
	Pair    string
	Price   string
	Side    string
	Size    string
	Type    string
	IsLocal bool
	Cancel  bool
	Csv     bool
	Status  string
}
