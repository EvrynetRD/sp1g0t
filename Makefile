API=./openapi/floodgate.yaml

validate-api:
	openapi-generator validate -i ${API}

generate: generate-models build

generate-models: validate-api
	openapi-generator generate -i ${API} -g go-gin-server -o models --additional-properties packageName=models
	rm -rf ./models/.openapi-generator
	rm -rf ./models/api
	rm ./models/.openapi-generator-ignore
	rm ./models/Dockerfile
	rm ./models/main.go
	rm ./models/go/api_default.go
	rm ./models/go/routers.go
	cp ./models/go/*.go ./models
	rm -rf ./models/go
	go fmt ./models/*
	for file in ./models/model_*.go; do mv -v "$$file" "./models/$${file#*_}";done

build: 
	go build -o bin/sp1g0t

remove-generated-go-models:
	find ./models/ -type f -not -name 'config.go' -delete

regenerate: remove-generated-go-models generate

test:
	go test ./...