package main

import (
	"os"
	"os/signal"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/EvrynetRD/sp1g0t/aws"
	"gitlab.com/EvrynetRD/sp1g0t/bot"
	"gitlab.com/EvrynetRD/sp1g0t/cmd"
	"gitlab.com/EvrynetRD/sp1g0t/db"
	"gitlab.com/EvrynetRD/sp1g0t/db/migrations"
	"gitlab.com/EvrynetRD/sp1g0t/db/repository"
	"gitlab.com/EvrynetRD/sp1g0t/hydra"
	_ "gitlab.com/EvrynetRD/sp1g0t/logger"
	"gitlab.com/EvrynetRD/sp1g0t/providers"
	"gitlab.com/EvrynetRD/sp1g0t/providers/coingecko"
	"gitlab.com/EvrynetRD/sp1g0t/providers/gateio"
	"gitlab.com/EvrynetRD/sp1g0t/utils"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		logrus.Fatal("Error loading .env file")
	}

	gormDB := db.New(db.GetDBAuthFromEnv())

	migrator := db.NewMigration(db.SqlDBFromGorm(gormDB))
	err = migrations.RunMigrations(migrator)
	if err != nil {
		logrus.Fatal(err)
	}

	h := hydra.NewHydra(utils.HTTP_CLIENT_DELAY, os.Getenv(utils.SOLANA_HOST), os.Getenv(utils.SOLANA_PRIVATE_KEY), os.Getenv(utils.WALLET))

	coingecko := coingecko.NewCoingecko(&coingecko.CoingeckoApi{})
	gateIO := gateio.NewGateIO(&gateio.GateeIOApi{})
	priceProvider := providers.NewPriceProvider(coingecko, gateIO)

	orderRepo := repository.NewOrderRepository(gormDB)
	settleRepo := repository.NewSettlesRepository(gormDB)
	priceRepo := repository.NewPricesRepository(gormDB)
	walletBalanceRepo := repository.NewWalletBalancesRepository(gormDB)
	repo := repository.NewRepository(orderRepo, settleRepo, priceRepo, walletBalanceRepo)
	awsSES := aws.NewSes()

	svc := bot.NewSp1g0tBot(&h, nil, repo, priceProvider, awsSES)

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	go func() {
		<-signals
		signal.Stop(signals)
		logrus.Info("\nCTRL-C command received. Exiting...")
		os.Exit(0)
	}()

	cmd.Execute(svc)
}
